<?php
session_start();
require_once __DIR__ . '/twitteroauth/twitteroauth.php';

$host = $_SERVER['HTTP_HOST'];
$path = substr(__FILE__, strlen($_SERVER['DOCUMENT_ROOT']));
$oauth_callback = "http://{$host}/{$path}";

$consumerKey = "";
$consumerSecret = "";
$accessKey = "";
$accessSecret = "";

if (isset($_REQUEST['oauth_verifier']) && isset($_SESSION['oauth_token'])) {
    $consumerKey = $_SESSION['consumer_key'];
    $consumerSecret = $_SESSION['consumer_secret'];

    $connection = new TwitterOAuth($_SESSION['consumer_key'], $_SESSION['consumer_secret'], $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
    $tokens = $connection->getAccessToken($_REQUEST['oauth_verifier']);
    $accessKey = $tokens['oauth_token'];
    $accessSecret = $tokens['oauth_token_secret'];
    session_destroy();
}

if (isset($_POST['consumer_key'])) {

    if ($_POST['consumer_key'] != "" && $_POST['consumer_secret'] != "") {
        $connection = new TwitterOAuth($_POST['consumer_key'], $_POST['consumer_secret']);
        $accessToken = $connection->getRequestToken($oauth_callback);

        switch ($connection->http_code) {
            case 200:
                $_SESSION['consumer_key'] = $consumerKey = $_POST['consumer_key'];
                $_SESSION['consumer_secret'] = $consumerSecret = $_POST['consumer_secret'];
                $_SESSION['oauth_token'] = $accessToken['oauth_token'];
                $_SESSION['oauth_token_secret'] = $accessToken['oauth_token_secret'];
                $url = $connection->getAuthorizeURL($accessToken['oauth_token']);
                header("Location:{$url}");
                break;
            default:
                $error = TRUE;
                break;
        }
    }
}
?>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="msvalidate.01" content="656DCADC9A1E19574640EF538CF46F5E" />

        <link href="css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/sweetalert.css" rel="stylesheet" type="text/css">

        <!-- Custom Fonts -->
        <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
        <link href="css/outer-style.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- jQuery Version 1.11.0 -->
        <script src="js/jquery-2.2.3.min.js"></script>
        <script src="js/sweetalert.min.js"></script>
    </head>
    <body>
        <section class="row1" >
            <div class="container ">
                <div class="row">
                    <h1 class="tagline text-center white hidd en-xs">Get your Twitter Access Tokens</h1>
                    <hr>
                    <div class="my-lg-gutter hidden-xs"></div>
                    <div class="my-lg-gutter hidden-xs"></div>
                    <form class="" role="form" method="post" id="access_token_form">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="white" for="consumer_key">Consumer Key</label>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-user"></i></span>
                                    <input type="text" class="form-control" name="consumer_key" id="consumer_key" value="<?= $consumerKey ?>" placeholder="Consumer Key"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="white" for="consumer_secret">Consumer Secret</label>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-user-secret"></i></span>
                                    <input type="text" class="form-control" name="consumer_secret" id="consumer_secret" value="<?= $consumerSecret ?>" placeholder="Consumer Secret"/>
                                </div>
                            </div>
                        </div>

                        <?php if ($accessKey != "" && $accessSecret != ""): ?>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="white" for="access_key">Access Token</label>
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1"><i class="fa fa-bookmark"></i></span>
                                        <input type="text" class="form-control" name="access_key" id="access_key" value="<?= $accessKey ?>" placeholder="Access Key"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="white" for="access_secret">Access Token Secret</label>
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1"><i class="fa fa-user-secret"></i></span>
                                        <input type="text" class="form-control" name="access_secret" id="access_secret" value="<?= $accessSecret ?>" placeholder="Access Secret"/>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-100-percent btn-info" id="btn_submit">Get Access Tokens <i class="fa fa-spin fa-spinner hidden"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </section>

        <script>
            $(function () {

                $("#access_token_form").submit(function () {
                    if ($("#consumer_key").val() == "" || $("#consumer_secret").val() == "") {
                        sweetAlert("Error", "We neeed consumer key and consumer secret to get your access tokens.", "error");
                        return false;
                    }
                    $("i.fa-spin").removeClass("hidden");
                });

                if (window.location.href.indexOf('?') > -1) {
                    history.pushState('', document.title, window.location.pathname);
                }

            });
        </script>
    </body>
</html>